import React from "react";

import { Button, Img, Text } from "components";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const Etape1FivePage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col font-leaguespartan items-center justify-start mx-auto w-full">
        <div className="md:h-[58px] h-[60px] md:px-5 relative w-full">
          <Text
            className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
            size="txtLeagueSpartanMedium20"
          >
            Découvrez la stratégie infaillible pour vendre vos produits grâce
            aux shortvidéos
          </Text>
          <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
            <Text
              className="mb-[5px] text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
          </div>
        </div>
        <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row md:gap-5 items-center justify-start p-3.5 w-full">
          <Img
            className="h-12 w-12"
            src="images/img_iconparkhamburgerbutton.svg"
            alt="iconparkhamburg"
          />
          <Text
            className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
            size="txtLeagueSpartanBold24"
          >
            create for me
          </Text>
          <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
          <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
        </div>
        <div className="flex flex-col font-montserrat md:gap-10 gap-[82px] items-center justify-start mt-[62px] md:px-5 py-[3px] w-[33%] md:w-full">
          <div className="flex flex-col items-center justify-start w-full">
            <div className="flex flex-col items-center justify-start w-full">
              <div className="flex flex-col items-center justify-start w-full">
                <div className="flex sm:flex-col flex-row gap-[23px] items-start justify-between w-full">
                  <Text
                    className="text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
                    size="txtMontserratRomanSemiBold30"
                  >
                    Etape 1/5
                  </Text>
                  <Img
                    className="h-[26px] md:h-auto sm:mt-0 mt-1 object-cover"
                    src="images/img_group642.png"
                    alt="group642"
                  />
                </div>
              </div>
            </div>
          </div>
          <Text
            className="mb-[7px] text-3xl sm:text-[26px] md:text-[28px] text-teal-400"
            size="txtMontserratRomanSemiBold30Teal400"
          >
            L’objectif de tes réseaux ?
          </Text>
        </div>
        <div className="flex flex-col font-montserrat items-center justify-start mt-[73px] md:px-5 w-1/4 md:w-full">
          <div
            className="bg-cover bg-no-repeat flex flex-col h-[50px] items-center justify-end p-2.5 rounded-[25px] w-full"
            style={{ backgroundImage: "url('images/img_group651.png')" }}
          >
            <Text
              className="mt-[3px] text-blue_gray-800 text-xl"
              size="txtMontserratRomanRegular20"
            >
              Produit physiques
            </Text>
          </div>
        </div>
        <div
          className="bg-cover bg-no-repeat flex flex-col font-montserrat h-[50px] items-center justify-end mt-[45px] p-2.5 md:px-5"
          style={{ backgroundImage: "url('images/img_group651.png')" }}
        >
          <Text
            className="mt-[3px] text-blue_gray-800 text-xl"
            size="txtMontserratRomanRegular20"
          >
            Produit numérique
          </Text>
        </div>
        <div
          className="bg-cover bg-no-repeat flex flex-col font-montserrat h-[50px] items-center justify-start mt-[30px] p-[11px] md:px-5"
          style={{ backgroundImage: "url('images/img_group651.png')" }}
        >
          <Text
            className="mb-0.5 text-blue_gray-800 text-xl"
            size="txtMontserratRomanRegular20"
          >
            Service
          </Text>
        </div>
        <div
          className="bg-cover bg-no-repeat flex flex-col font-montserrat h-[50px] items-center justify-start mt-[31px] p-2.5 md:px-5"
          style={{ backgroundImage: "url('images/img_group651.png')" }}
        >
          <Text
            className="mb-[3px] text-blue_gray-800 text-xl"
            size="txtMontserratRomanRegular20"
          >
            Événement
          </Text>
        </div>
        <div
          className="bg-cover bg-no-repeat flex flex-col font-montserrat h-[50px] items-center justify-end mt-[31px] p-3 md:px-5"
          style={{ backgroundImage: "url('images/img_group651.png')" }}
        >
          <Text
            className="text-blue_gray-800 text-xl"
            size="txtMontserratRomanRegular20"
          >
            Autre
          </Text>
        </div>
        <div className="flex flex-col font-leaguespartan mt-[101px] md:px-5 relative w-full">
          <div className="bg-blue_gray-800 flex flex-col items-start justify-end mx-auto p-14 md:px-10 sm:px-5 rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full">
            <div className="flex flex-col items-start justify-start ml-6 md:ml-[0] mt-[68px] w-[29%] md:w-full">
              <Text
                className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
                size="txtLeagueSpartanBold32"
              >
                Create for me
              </Text>
              <div className="flex flex-col font-montserrat gap-[37px] items-start justify-start mt-[45px] w-full">
                <Text
                  className="text-orange-50 text-sm"
                  size="txtMontserratRomanBold14"
                >
                  Enterprise
                </Text>
                <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                  <Text
                    className="text-orange-50 text-sm"
                    size="txtMontserratRomanMedium14"
                  >
                    Contactor
                  </Text>
                  <Text
                    className="text-orange-50 text-sm"
                    size="txtMontserratRomanMedium14"
                  >
                    Les avis
                  </Text>
                  <Text
                    className="text-orange-50 text-sm"
                    size="txtMontserratRomanMedium14"
                  >
                    Programmer d’affiliation
                  </Text>
                </div>
              </div>
              <Img
                className="h-[22px] md:h-auto mt-[37px] object-cover"
                src="images/img_group688.png"
                alt="group688"
              />
            </div>
          </div>
          <Text
            className="bg-blue_gray-800 border-blue_gray-100 border-solid border-t h-[81px] justify-center max-w-[1440px] md:max-w-full mt-[-27px] mx-auto pb-[35px] pt-8 sm:px-5 px-[35px] text-shadow-ts1 text-sm text-white-A700 w-full z-[1]"
            size="txtLeagueSpartanMedium14"
          >
            Copyright@2023createforme
          </Text>
        </div>
      </div>
    </>
  );
};

export default Etape1FivePage;
