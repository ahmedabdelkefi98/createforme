import React from "react";

const sizeClasses = {
  txtLeagueSpartanBold55: "font-bold font-leaguespartan",
  txtLeagueSpartanBold32: "font-bold font-leaguespartan",
  txtLeagueSpartanBold36: "font-bold font-leaguespartan",
  txtInterRegular20: "font-inter font-normal",
  txtMontserratRomanRegular14: "font-montserrat font-normal",
  txtMontserratRomanSemiBold20Orange200: "font-montserrat font-semibold",
  txtMontserratRomanMedium20Bluegray800: "font-medium font-montserrat",
  txtLeagueSpartanBold30: "font-bold font-leaguespartan",
  txtMontserratRomanSemiBold36: "font-montserrat font-semibold",
  txtMontserratRomanMedium14: "font-medium font-montserrat",
  txtLeagueSpartanMedium22: "font-leaguespartan font-medium",
  txtMontserratRomanMedium16: "font-medium font-montserrat",
  txtMontserratRomanSemiBold32: "font-montserrat font-semibold",
  txtMontserratRomanMedium18: "font-medium font-montserrat",
  txtMontserratRomanSemiBold30: "font-montserrat font-semibold",
  txtMontserratRomanSemiBold96: "font-montserrat font-semibold",
  txtMontserratRomanSemiBold20Teal400: "font-montserrat font-semibold",
  txtLeagueSpartanRegular20: "font-leaguespartan font-normal",
  txtLeagueSpartanMedium20: "font-leaguespartan font-medium",
  txtMontserratRomanSemiBold18: "font-montserrat font-semibold",
  txtLeagueSpartanMedium20Teal400: "font-leaguespartan font-medium",
  txtMontserratRomanRegular16Bluegray800: "font-montserrat font-normal",
  txtMontserratRomanSemiBold24Teal400: "font-montserrat font-semibold",
  txtLeagueSpartanBold24: "font-bold font-leaguespartan",
  txtMontserratRomanBold14: "font-bold font-montserrat",
  txtMontserratRomanSemiBold18Bluegray800: "font-montserrat font-semibold",
  txtMontserratRomanSemiBold40: "font-montserrat font-semibold",
  txtMontserratRomanRegular20: "font-montserrat font-normal",
  txtMontserratRomanSemiBold30Teal400: "font-montserrat font-semibold",
  txtMontserratRomanRegular16: "font-montserrat font-normal",
  txtLeagueSpartanSemiBold36: "font-leaguespartan font-semibold",
  txtMontserratRomanSemiBold24: "font-montserrat font-semibold",
  txtMontserratRomanSemiBold18OrangeA200: "font-montserrat font-semibold",
  txtLeagueSpartanMedium14: "font-leaguespartan font-medium",
  txtMontserratRomanSemiBold20: "font-montserrat font-semibold",
  txtMontserratRomanMedium20: "font-medium font-montserrat",
  txtLeagueSpartanSemiBold30: "font-leaguespartan font-semibold",
  txtLeagueSpartanMedium20Gray800: "font-leaguespartan font-medium",
  txtMontserratRomanMedium24: "font-medium font-montserrat",
  txtLeagueSpartanSemiBold32: "font-leaguespartan font-semibold",
  txtMontserratRomanMedium20Black900: "font-medium font-montserrat",
} as const;

export type TextProps = Partial<{
  className: string;
  size: keyof typeof sizeClasses;
  as: any;
}> &
  React.DetailedHTMLProps<
    React.HTMLAttributes<HTMLSpanElement>,
    HTMLSpanElement
  >;

const Text: React.FC<React.PropsWithChildren<TextProps>> = ({
  children,
  className = "",
  size,
  as,
  ...restProps
}) => {
  const Component = as || "p";

  return (
    <Component
      className={`text-left ${className} ${size && sizeClasses[size]}`}
      {...restProps}
    >
      {children}
    </Component>
  );
};

export { Text };
