import React from "react";

import { Button, Img, Text } from "components";
import Footer from "components/Footer";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const Etape4FivePage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col items-center justify-start mx-auto w-full">
        <div className="flex flex-col items-start justify-start w-full">
          <div className="font-leaguespartan md:h-[58px] h-[60px] md:px-5 relative w-full">
            <Text
              className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
            <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
              <Text
                className="mb-[5px] text-center text-white-A700 text-xl"
                size="txtLeagueSpartanMedium20"
              >
                Découvrez la stratégie infaillible pour vendre vos produits
                grâce aux shortvidéos
              </Text>
            </div>
          </div>
          <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row font-leaguespartan md:gap-5 items-center justify-start p-3.5 w-full">
            <Img
              className="h-12 w-12"
              src="images/img_iconparkhamburgerbutton.svg"
              alt="iconparkhamburg"
            />
            <Text
              className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
              size="txtLeagueSpartanBold24"
            >
              create for me
            </Text>
            <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
            <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
          </div>
          <div className="font-montserrat md:h-[233px] h-[264px] sm:h-[272px] md:ml-[0] ml-[391px] mt-16 md:px-5 relative w-[46%] md:w-full">
            <div className="absolute flex flex-col inset-x-[0] items-center justify-start mx-auto py-[3px] top-[0] w-[71%]">
              <div className="flex flex-col items-center justify-start mb-[126px] w-full">
                <div className="flex flex-col items-center justify-start w-full">
                  <div className="flex flex-col items-center justify-start w-full">
                    <div className="flex sm:flex-col flex-row gap-[13px] items-start justify-between w-full">
                      <Text
                        className="text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
                        size="txtMontserratRomanSemiBold30"
                      >
                        Etape 4/5
                      </Text>
                      <Img
                        className="h-[26px] md:h-auto sm:mt-0 mt-1 object-cover"
                        src="images/img_group642.png"
                        alt="group642"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Text
              className="absolute bottom-[0] inset-x-[0] leading-[142.01%] mx-auto text-4xl sm:text-[32px] md:text-[34px] text-center text-teal-400"
              size="txtMontserratRomanSemiBold36"
            >
              <>
                Sur quelles plateforme voulez vous <br />
                développer votre communauté ?
              </>
            </Text>
          </div>
          <div className="flex sm:flex-col flex-row sm:gap-10 items-center justify-between md:ml-[0] ml-[435px] mt-[122px] md:px-5 w-[44%] md:w-full">
            <div className="bg-indigo-600 flex flex-col h-[116px] items-end justify-end sm:mt-0 mt-0.5 pt-[17px] px-[17px] rounded-md w-[116px]">
              <Img
                className="h-[98px]"
                src="images/img_facebook.svg"
                alt="facebook"
              />
            </div>
            <div className="flex flex-col items-center justify-start mb-0.5 w-[62%] sm:w-full">
              <div className="flex flex-row items-center justify-between w-full">
                <Img
                  className="h-[110px]"
                  src="images/img_logosyoutubeicon.svg"
                  alt="logosyoutubeico"
                />
                <Img
                  className="h-[116px] md:h-auto object-cover w-[116px]"
                  src="images/img_skilliconsinstagram.png"
                  alt="skilliconsinsta"
                />
              </div>
            </div>
          </div>
          <Text
            className="bg-clip-text bg-gradient  md:ml-[0] ml-[433px] mt-[138px] md:text-3xl sm:text-[28px] text-[32px] text-center text-transparent"
            size="txtMontserratRomanSemiBold32"
          >
            Je vous laisse choisir, peut importe
          </Text>
          <Footer className="bg-blue_gray-800 flex font-leaguespartan items-center justify-center mt-[100px] md:px-5 rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full" />
        </div>
      </div>
    </>
  );
};

export default Etape4FivePage;
