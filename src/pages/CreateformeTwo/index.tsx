import React from "react";

import { Button, Img, Input, Line, Text } from "components";
import Footer from "components/Footer";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const CreateformeTwoPage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col font-leaguespartan items-center justify-start mx-auto w-full">
        <div className="flex flex-col items-start justify-start w-full">
          <div className="md:h-[58px] h-[60px] md:px-5 relative w-full">
            <Text
              className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
            <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
              <Text
                className="mb-[5px] text-center text-white-A700 text-xl"
                size="txtLeagueSpartanMedium20"
              >
                Découvrez la stratégie infaillible pour vendre vos produits
                grâce aux shortvidéos
              </Text>
            </div>
          </div>
          <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row md:gap-5 items-center justify-start p-3.5 w-full">
            <Img
              className="h-12 w-12"
              src="images/img_iconparkhamburgerbutton.svg"
              alt="iconparkhamburg"
            />
            <Text
              className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
              size="txtLeagueSpartanBold24"
            >
              create for me
            </Text>
            <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
            <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
          </div>
          <div className="flex md:flex-col flex-row font-montserrat md:gap-5 items-center justify-start ml-20 md:ml-[0] mt-16 md:px-5 w-[83%] md:w-full">
            <div className="flex flex-col md:gap-10 gap-[83px] items-start justify-start w-auto sm:w-full">
              <div className="flex flex-col gap-[22px] items-start justify-start w-full">
                <Img
                  className="h-[43px] md:h-auto object-cover"
                  src="images/img_group611.png"
                  alt="group611"
                />
                <Text
                  className="leading-[142.01%] text-blue_gray-800 text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  <>
                    Une stratégie qui inclura également une <br />
                    analyse approfondie de votre concurrence.
                  </>
                </Text>
              </div>
              <div className="flex flex-col gap-[22px] items-start justify-start w-[91%] md:w-full">
                <Img
                  className="h-[47px] md:h-auto object-cover"
                  src="images/img_vector_47x60.png"
                  alt="vector"
                />
                <Text
                  className="leading-[142.01%] text-blue_gray-800 text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  <>
                    L’outil permet d&#39;automatiser ta stratégie <br />
                    et ton contenu tout en restant authentique.
                  </>
                </Text>
              </div>
              <div className="flex flex-col gap-[22px] items-start justify-start w-[79%] md:w-full">
                <Img
                  className="h-[45px] md:h-auto object-cover"
                  src="images/img_vector_45x60.png"
                  alt="vector_One"
                />
                <Text
                  className="leading-[142.01%] text-blue_gray-800 text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  <>
                    Un montage vidéo automatique pour <br />
                    une expérience sans effort
                  </>
                </Text>
              </div>
              <div className="flex flex-col gap-[22px] items-start justify-start w-[83%] md:w-full">
                <div className="border-[5px] border-solid flex flex-col items-center justify-start orange_200_blue_gray_800_border6 p-1.5 rounded-sm w-[15%] md:w-full">
                  <Img
                    className="h-[31px] md:h-auto object-cover"
                    src="images/img_group615.png"
                    alt="group615"
                  />
                </div>
                <Text
                  className="leading-[142.01%] text-blue_gray-800 text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  <>
                    Gagnez du temps en vous focalisant sur <br />
                    l&#39;essence même de votre métier.
                  </>
                </Text>
              </div>
            </div>
            <Line className="bg-gray-300 h-[753px] md:h-px md:ml-[0] ml-[148px] md:mt-0 mt-2 md:w-full w-px" />
            <div className="flex flex-col justify-start md:ml-[0] ml-[98px] w-[38%] md:w-full">
              <Text
                className="text-2xl md:text-[22px] text-blue_gray-800 sm:text-xl"
                size="txtMontserratRomanSemiBold24"
              >
                Créez votre compte maintenant
              </Text>
              <div className="flex flex-col gap-6 items-start justify-start mt-12 w-[97%] md:w-full">
                <Text
                  className="md:ml-[0] ml-[3px] text-blue_gray-800 text-xl"
                  size="txtMontserratRomanSemiBold20"
                >
                  Email
                </Text>
                <Input
                  name="group619"
                  placeholder="Entrez votre adresse e-mail ici"
                  className="p-0 placeholder:text-gray-800 text-left text-sm w-full"
                  wrapClassName="bg-transparent w-full"
                  type="email"
                  color="orange_200_blue_gray_800"
                ></Input>
              </div>
              <div className="h-[149px] md:h-[223px] mt-[73px] relative w-full">
                <div className="absolute flex flex-col gap-[21px] h-full inset-[0] items-start justify-center m-auto w-[97%]">
                  <Text
                    className="text-blue_gray-800 text-xl"
                    size="txtMontserratRomanSemiBold20"
                  >
                    Mot de passe
                  </Text>
                  <Input
                    name="groupFive"
                    placeholder="Entrez votre mot de passe ici"
                    className="p-0 placeholder:text-gray-800 text-left text-sm w-full"
                    wrapClassName="bg-transparent w-full"
                    color="orange_200_blue_gray_800"
                  ></Input>
                  <div className="border border-solid h-[23px] md:ml-[0] ml-[3px] orange_200_blue_gray_800_border7 rounded-sm w-[23px]"></div>
                </div>
                <Text
                  className="absolute bottom-[0] right-[0] text-blue_gray-800 text-sm"
                  size="txtMontserratRomanRegular14"
                >
                  En vous inscrivant, vous acceptez les conditions générales.
                </Text>
              </div>
              <div
                className="bg-cover bg-no-repeat flex flex-col h-[55px] items-center justify-start md:ml-[0] ml-[125px] mt-[57px] p-3.5"
                style={{ backgroundImage: "url('images/img_group622.svg')" }}
              >
                <Text
                  className="text-blue_gray-800 text-center text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  C’est Parti
                </Text>
              </div>
              <div className="flex flex-row gap-3.5 items-start justify-center md:ml-[0] ml-[70px] mt-[35px] w-[70%] md:w-full">
                <Line className="bg-blue_gray-800 h-px mb-[13px] mt-2.5 w-[43%]" />
                <Text
                  className="text-blue_gray-800 text-center text-xl"
                  size="txtMontserratRomanMedium20Bluegray800"
                >
                  or
                </Text>
                <Line className="bg-blue_gray-800 h-px mb-[13px] mt-2.5 w-[43%]" />
              </div>
              <div className="flex flex-col items-center justify-start md:ml-[0] ml-[66px] mt-[31px] w-[71%] md:w-full">
                <div
                  className="bg-cover bg-no-repeat flex flex-col h-[55px] items-center justify-end p-3.5 w-full"
                  style={{ backgroundImage: "url('images/img_group623.png')" }}
                >
                  <div className="flex flex-row gap-2 items-center justify-start w-[91%] md:w-full">
                    <Img
                      className="h-[25px]"
                      src="images/img_logosgooglegmail.svg"
                      alt="logosgooglegmai"
                    />
                    <Text
                      className="text-blue_gray-800 text-center text-xl"
                      size="txtMontserratRomanMedium20Bluegray800"
                    >
                      Continuer avec Gmail
                    </Text>
                  </div>
                </div>
              </div>
              <Text
                className="md:ml-[0] ml-[29px] mt-[65px] text-blue_gray-800 text-center text-lg"
                size="txtMontserratRomanSemiBold18Bluegray800"
              >
                <span className="text-blue_gray-800 font-montserrat font-semibold">
                  Si vous avez déjà un compte
                </span>
                <span className="text-blue_gray-800 font-montserrat font-semibold">
                  ?
                </span>
                <span className="text-blue_gray-800 font-montserrat font-semibold">
                  {" "}
                </span>
                <span className="text-orange-200 font-montserrat font-semibold">
                  se connecter
                </span>
              </Text>
            </div>
          </div>
          <Footer className="bg-blue_gray-800 flex font-leaguespartan items-center justify-center mt-[97px] md:px-5 rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full" />
        </div>
      </div>
    </>
  );
};

export default CreateformeTwoPage;
