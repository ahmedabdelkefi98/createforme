import React from "react";

import { Img, Text } from "components";

type FooterProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
> &
  Partial<{}>;

const Footer: React.FC<FooterProps> = (props) => {
  return (
    <>
      <footer className={props.className}>
        <div className="flex flex-col gap-[47px] items-center justify-center mt-[52px] w-full">
          <div className="flex flex-col items-start justify-start ml-20 md:ml-[0] mr-[982px] w-[27%] md:w-full">
            <Text
              className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
              size="txtLeagueSpartanBold32"
            >
              Create for me
            </Text>
            <div className="flex flex-col gap-[37px] items-start justify-start mt-[45px] w-full">
              <Text
                className="text-orange-50 text-sm"
                size="txtMontserratRomanBold14"
              >
                Enterprise
              </Text>
              <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                <Text
                  className="text-orange-50 text-sm"
                  size="txtMontserratRomanMedium14"
                >
                  Contacter
                </Text>
                <Text
                  className="text-orange-50 text-sm"
                  size="txtMontserratRomanMedium14"
                >
                  Les avis
                </Text>
                <Text
                  className="text-orange-50 text-sm"
                  size="txtMontserratRomanMedium14"
                >
                  Programmer d’affiliation
                </Text>
              </div>
            </div>
            <Img
              className="h-[22px] md:h-auto mt-[37px] object-cover"
              src="images/img_group688.png"
              alt="group688"
            />
          </div>
          <div className="bg-blue_gray-800 border-blue_gray-100 border-solid border-t flex flex-col items-start justify-start p-[30px] sm:px-5 shadow-bs1 w-full">
            <div className="flex flex-col items-center justify-start mb-[7px] md:ml-[0] ml-[49px]">
              <Text
                className="text-sm text-white-A700"
                size="txtLeagueSpartanMedium14"
              >
                Copyright@2023createforme
              </Text>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
};

Footer.defaultProps = {};

export default Footer;
