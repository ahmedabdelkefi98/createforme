import React from "react";

import { Img, Text } from "components";

type HomepageColumnProps = Omit<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  "dynamictext"
> &
  Partial<{ dynamictext: string }>;

const HomepageColumn: React.FC<HomepageColumnProps> = (props) => {
  return (
    <>
      <div className={props.className}>
        <div className="flex flex-col items-center justify-start w-full">
          <div className="flex flex-row items-start justify-evenly w-full">
            <Text
              className="text-black-900 text-center text-xl"
              size="txtMontserratRomanMedium20Black900"
            >
              {props?.dynamictext}
            </Text>
            <Img
              className="h-6 w-6"
              src="images/img_arrowdown.svg"
              alt="arrowdown"
            />
          </div>
        </div>
      </div>
    </>
  );
};

HomepageColumn.defaultProps = { dynamictext: "FR" };

export default HomepageColumn;
