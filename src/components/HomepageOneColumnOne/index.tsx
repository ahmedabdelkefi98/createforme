import React from "react";

import { Img } from "components";

type HomepageOneColumnOneProps = React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLDivElement>,
  HTMLDivElement
> &
  Partial<{}>;

const HomepageOneColumnOne: React.FC<HomepageOneColumnOneProps> = (props) => {
  return (
    <>
      <div className={props.className}>
        <div className="flex flex-col gap-[51px] items-center justify-start my-[37px] w-auto">
          <Img
            className="h-10 w-10"
            src="images/img_icomoonfreehome.svg"
            alt="icomoonfreehome"
          />
          <Img
            className="h-[30px] w-[31px]"
            src="images/img_signal.svg"
            alt="signal"
          />
          <Img
            className="h-6 w-[41px]"
            src="images/img_settings_gray_300.svg"
            alt="settings"
          />
          <Img
            className="h-[30px] w-9"
            src="images/img_question.svg"
            alt="question"
          />
          <Img className="h-7 w-8" src="images/img_profile.svg" alt="profile" />
        </div>
      </div>
    </>
  );
};

HomepageOneColumnOne.defaultProps = {};

export default HomepageOneColumnOne;
