import React from "react";

import { Button, Img, Text } from "components";
import Footer from "components/Footer";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const JecrermastratgiePage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col font-leaguespartan items-center justify-start mx-auto w-full">
        <div className="md:h-[58px] h-[60px] md:px-5 relative w-full">
          <Text
            className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
            size="txtLeagueSpartanMedium20"
          >
            Découvrez la stratégie infaillible pour vendre vos produits grâce
            aux shortvidéos
          </Text>
          <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
            <Text
              className="mb-[5px] text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
          </div>
        </div>
        <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row md:gap-5 items-center justify-start p-3.5 w-full">
          <Img
            className="h-12 w-12"
            src="images/img_iconparkhamburgerbutton.svg"
            alt="iconparkhamburg"
          />
          <Text
            className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
            size="txtLeagueSpartanBold24"
          >
            create for me
          </Text>
          <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
          <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
        </div>
        <div className="flex flex-col font-montserrat gap-11 items-center justify-start max-w-[1042px] mt-[152px] mx-auto md:px-5 w-full">
          <Text
            className="bg-clip-text bg-gradient  md:text-5xl text-8xl text-shadow-ts text-transparent"
            size="txtMontserratRomanSemiBold96"
          >
            Je créer ma stratégie
          </Text>
          <div
            className="bg-cover bg-no-repeat flex flex-col h-[83px] items-center justify-end p-4"
            style={{ backgroundImage: "url('images/img_group622.svg')" }}
          >
            <Text
              className="sm:text-4xl md:text-[38px] text-[40px] text-blue_gray-800 text-center"
              size="txtMontserratRomanSemiBold40"
            >
              GO !
            </Text>
          </div>
        </div>
        <Footer className="bg-blue_gray-800 flex font-leaguespartan items-center justify-center mt-[185px] md:px-5 rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full" />
      </div>
    </>
  );
};

export default JecrermastratgiePage;
