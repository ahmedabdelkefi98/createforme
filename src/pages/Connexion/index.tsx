import React from "react";

import { Button, Img, Input, Line, Text } from "components";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const ConnexionPage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col items-center justify-end mx-auto w-full">
        <div className="font-leaguespartan md:h-[58px] h-[60px] md:px-5 relative w-full">
          <Text
            className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
            size="txtLeagueSpartanMedium20"
          >
            Découvrez la stratégie infaillible pour vendre vos produits grâce
            aux shortvidéos
          </Text>
          <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
            <Text
              className="mb-[5px] text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
          </div>
        </div>
        <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row font-leaguespartan md:gap-5 items-center justify-start p-3.5 w-full">
          <Img
            className="h-12 w-12"
            src="images/img_iconparkhamburgerbutton.svg"
            alt="iconparkhamburg"
          />
          <Text
            className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
            size="txtLeagueSpartanBold24"
          >
            create for me
          </Text>
          <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
          <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
        </div>
        <Text
          className="mt-[63px] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
          size="txtMontserratRomanSemiBold30"
        >
          Connexion
        </Text>
        <div className="font-montserrat h-[678px] md:h-[726px] mt-[54px] md:px-5 relative w-[31%] sm:w-full">
          <Text
            className="absolute h-max inset-y-[0] my-auto right-[0] text-blue_gray-800 text-sm"
            size="txtMontserratRomanRegular14"
          >
            En vous inscrivant, vous acceptez les conditions générales.
          </Text>
          <div className="absolute flex flex-col h-full inset-[0] items-center justify-center m-auto w-[97%]">
            <div className="flex flex-col gap-6 items-center justify-start w-full">
              <Text
                className="text-blue_gray-800 text-xl"
                size="txtMontserratRomanMedium20Bluegray800"
              >
                Email
              </Text>
              <Input
                name="group619"
                placeholder="Entrez votre adresse e-mail ici"
                className="p-0 placeholder:text-gray-800 text-left text-sm w-full"
                wrapClassName="bg-transparent w-full"
                type="email"
                color="orange_200_blue_gray_800"
              ></Input>
            </div>
            <div className="flex flex-col gap-[21px] items-center justify-start mt-[73px] w-full">
              <Text
                className="text-blue_gray-800 text-xl"
                size="txtMontserratRomanMedium20Bluegray800"
              >
                Mot de passe
              </Text>
              <div className="flex flex-col gap-5 items-start justify-start w-full">
                <Input
                  name="groupThirtyThree"
                  placeholder="Entrez votre mot de passe ici"
                  className="p-0 placeholder:text-gray-800 text-left text-sm w-full"
                  wrapClassName="bg-transparent w-full"
                  size="sm"
                  color="orange_200_blue_gray_800"
                ></Input>
                <div className="border border-solid h-[23px] orange_200_blue_gray_800_border9 rounded-sm w-[23px]"></div>
              </div>
            </div>
            <div
              className="bg-cover bg-no-repeat flex flex-col h-[55px] items-center justify-start mt-[57px] p-3.5"
              style={{ backgroundImage: "url('images/img_group622.svg')" }}
            >
              <Text
                className="text-blue_gray-800 text-center text-xl"
                size="txtMontserratRomanMedium20Bluegray800"
              >
                C’est Parti
              </Text>
            </div>
            <div className="flex flex-row gap-3.5 items-start justify-center mt-[35px] w-[72%] md:w-full">
              <Line className="bg-blue_gray-800 h-px mb-[13px] mt-2.5 w-[43%]" />
              <Text
                className="text-blue_gray-800 text-center text-xl"
                size="txtMontserratRomanMedium20Bluegray800"
              >
                or
              </Text>
              <Line className="bg-blue_gray-800 h-px mb-[13px] mt-2.5 w-[43%]" />
            </div>
            <div className="flex flex-col items-center justify-start mt-[31px] w-[74%] md:w-full">
              <div
                className="bg-cover bg-no-repeat flex flex-col h-[55px] items-center justify-end p-3.5 w-full"
                style={{ backgroundImage: "url('images/img_group623.png')" }}
              >
                <div className="flex flex-row gap-2 items-center justify-start w-[91%] md:w-full">
                  <Img
                    className="h-[25px]"
                    src="images/img_logosgooglegmail.svg"
                    alt="logosgooglegmai"
                  />
                  <Text
                    className="text-blue_gray-800 text-center text-xl"
                    size="txtMontserratRomanMedium20Bluegray800"
                  >
                    Continuer avec Gmail
                  </Text>
                </div>
              </div>
            </div>
            <Text
              className="mt-[65px] text-blue_gray-800 text-center text-lg"
              size="txtMontserratRomanSemiBold18Bluegray800"
            >
              <span className="text-blue_gray-800 font-montserrat font-semibold">
                Si vous avez déjà un compte
              </span>
              <span className="text-blue_gray-800 font-montserrat font-semibold">
                ?
              </span>
              <span className="text-blue_gray-800 font-montserrat font-semibold">
                {" "}
              </span>
              <span className="text-orange-200 font-montserrat font-semibold">
                se connecter
              </span>
            </Text>
          </div>
        </div>
        <div className="flex flex-col font-leaguespartan items-center justify-end mt-[100px] w-full">
          <div className="flex flex-col gap-[46px] justify-start w-full">
            <div className="flex md:h-[387px] h-[392px] justify-end md:px-5 relative w-full">
              <div className="flex flex-row gap-[26px] h-full items-center justify-center mb-3.5 ml-[85px] mt-auto w-[6%]">
                <Img
                  className="h-[30px]"
                  src="images/img_logostiktokicon.svg"
                  alt="logostiktokicon"
                />
                <Img
                  className="h-[30px] md:h-auto object-cover w-[30px]"
                  src="images/img_group_white_a700.png"
                  alt="group"
                />
              </div>
              <div className="absolute bg-blue_gray-800 flex flex-col h-full inset-[0] items-center justify-center m-auto pt-[52px] rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full">
                <div className="flex flex-col gap-[47px] justify-start w-full">
                  <div className="flex flex-col items-start justify-start md:ml-[0] ml-[77px] w-[27%] md:w-full">
                    <Text
                      className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
                      size="txtLeagueSpartanBold32"
                    >
                      Create for me
                    </Text>
                    <div className="flex flex-col font-montserrat gap-[37px] items-start justify-start mt-[45px] w-full">
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanBold14"
                      >
                        Enterprise
                      </Text>
                      <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                        <Text
                          className="text-orange-50 text-sm"
                          size="txtMontserratRomanMedium14"
                        >
                          Contactor
                        </Text>
                        <Text
                          className="text-orange-50 text-sm"
                          size="txtMontserratRomanMedium14"
                        >
                          Les avis
                        </Text>
                        <Text
                          className="text-orange-50 text-sm"
                          size="txtMontserratRomanMedium14"
                        >
                          Programmer d’affiliation
                        </Text>
                      </div>
                    </div>
                    <Img
                      className="h-[22px] md:h-auto mt-[37px] object-cover"
                      src="images/img_group688.png"
                      alt="group688"
                    />
                  </div>
                  <div className="bg-blue_gray-800 border-blue_gray-100 border-solid border-t flex flex-col items-start justify-start p-[30px] sm:px-5 shadow-bs1 w-full">
                    <div className="flex flex-col items-center justify-start mb-[7px] md:ml-[0] ml-[46px]">
                      <Text
                        className="text-sm text-white-A700"
                        size="txtLeagueSpartanMedium14"
                      >
                        Copyright@2023createforme
                      </Text>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Text
              className="ml-24 md:ml-[0] text-center text-sm text-white-A700"
              size="txtLeagueSpartanMedium14"
            >
              Copyright@2023createforme
            </Text>
          </div>
        </div>
      </div>
    </>
  );
};

export default ConnexionPage;
