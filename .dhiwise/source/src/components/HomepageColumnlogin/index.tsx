import React from "react";

import { Button, Text } from "components";

type HomepageColumnloginProps = Omit<
  React.DetailedHTMLProps<React.HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
  "loginbutton" | "registertext"
> &
  Partial<{ loginbutton: string; registertext: string }>;

const HomepageColumnlogin: React.FC<HomepageColumnloginProps> = (props) => {
  return (
    <>
      <div className={props.className}>
        <div className="flex flex-row gap-0.5 items-center justify-between orange_200_blue_gray_800_border2 outline outline-[1px] sm:pr-5 pr-[22px] rounded-[27px] md:w-full">
          <Button
            className="!text-blue_gray-800 cursor-pointer font-medium font-montserrat leading-[normal] min-w-[130px] text-center text-xl"
            shape="round"
            color="orange_A200"
            size="sm"
            variant="fill"
          >
            {props?.loginbutton}
          </Button>
          <Text
            className="text-blue_gray-800 text-center text-xl"
            size="txtMontserratRomanMedium20Bluegray800"
          >
            {props?.registertext}
          </Text>
        </div>
      </div>
    </>
  );
};

HomepageColumnlogin.defaultProps = {
  loginbutton: "LogIn",
  registertext: "Register",
};

export default HomepageColumnlogin;
