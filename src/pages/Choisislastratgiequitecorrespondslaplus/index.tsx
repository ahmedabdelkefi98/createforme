import React from "react";

import { Button, Img, List, Text } from "components";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const ChoisislastratgiequitecorrespondslaplusPage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col items-center justify-start mx-auto pb-[3px] w-full">
        <div className="flex flex-col items-center justify-start w-full">
          <div className="font-leaguespartan md:h-[58px] h-[60px] md:px-5 relative w-full">
            <Text
              className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
            <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
              <Text
                className="mb-[5px] text-center text-white-A700 text-xl"
                size="txtLeagueSpartanMedium20"
              >
                Découvrez la stratégie infaillible pour vendre vos produits
                grâce aux shortvidéos
              </Text>
            </div>
          </div>
          <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row font-leaguespartan md:gap-5 items-center justify-start p-3.5 w-full">
            <Img
              className="h-12 w-12"
              src="images/img_iconparkhamburgerbutton.svg"
              alt="iconparkhamburg"
            />
            <Text
              className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
              size="txtLeagueSpartanBold24"
            >
              create for me
            </Text>
            <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
            <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
          </div>
          <Text
            className="mt-[62px] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
            size="txtMontserratRomanSemiBold30"
          >
            Choisis la stratégie qui te corresponds la plus
          </Text>
          <List
            className="sm:flex-col flex-row font-montserrat md:gap-10 gap-[88px] grid sm:grid-cols-1 md:grid-cols-2 grid-cols-3 justify-center max-w-7xl mt-[76px] mx-auto md:px-5 w-full"
            orientation="horizontal"
          >
            <div className="bg-gradient2  flex flex-1 flex-col items-center justify-center outline outline-[1px] outline-blue-50 p-3.5 rounded-[30px] shadow-bs w-full">
              <div className="flex flex-col gap-9 items-center justify-start my-11 w-full">
                <Text
                  className="text-teal-400 text-xl"
                  size="txtMontserratRomanSemiBold20Teal400"
                >
                  Stratégie 1 : “Le sushi du jour”
                </Text>
                <div className="flex flex-col gap-[34px] items-start justify-start w-full">
                  <div className="flex flex-row gap-2 items-start justify-start w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      30%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Mettez en avant un plat de vos sushi{" "}
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Session de cuisine
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3.5 items-start justify-start w-[72%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      15%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Un concours de recettes
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      L’art de la création
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3 items-start justify-start w-[79%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      10%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Partagez des faits amusant
                    </Text>
                  </div>
                  <div className="flex flex-row gap-[21px] items-start justify-start w-[41%] md:w-full">
                    <Text
                      className="h-[26px] text-lg text-orange-A200 text-shadow-ts w-[26px]"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      5%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Avis clients
                    </Text>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gradient2  flex flex-1 flex-col items-center justify-center outline outline-[1px] outline-blue-50 p-3.5 rounded-[30px] shadow-bs w-full">
              <div className="flex flex-col gap-9 items-center justify-start my-11 w-full">
                <Text
                  className="text-teal-400 text-xl"
                  size="txtMontserratRomanSemiBold20Teal400"
                >
                  Stratégie 1 : “Le sushi du jour”
                </Text>
                <div className="flex flex-col gap-[34px] items-start justify-start w-full">
                  <div className="flex flex-row gap-2 items-start justify-start w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      30%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Mettez en avant un plat de vos sushi{" "}
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Session de cuisine
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3.5 items-start justify-start w-[72%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      15%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Un concours de recettes
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      L’art de la création
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3 items-start justify-start w-[79%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      10%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Partagez des faits amusant
                    </Text>
                  </div>
                  <div className="flex flex-row gap-[21px] items-start justify-start w-[41%] md:w-full">
                    <Text
                      className="h-[26px] text-lg text-orange-A200 text-shadow-ts w-[26px]"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      5%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Avis clients
                    </Text>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gradient2  flex flex-1 flex-col items-center justify-center outline outline-[1px] outline-blue-50 p-3.5 rounded-[30px] shadow-bs w-full">
              <div className="flex flex-col gap-9 items-center justify-start my-11 w-full">
                <Text
                  className="text-teal-400 text-xl"
                  size="txtMontserratRomanSemiBold20Teal400"
                >
                  Stratégie 1 : “Le sushi du jour”
                </Text>
                <div className="flex flex-col gap-[34px] items-start justify-start w-full">
                  <div className="flex flex-row gap-2 items-start justify-start w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      30%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Mettez en avant un plat de vos sushi{" "}
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Session de cuisine
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3.5 items-start justify-start w-[72%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      15%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Un concours de recettes
                    </Text>
                  </div>
                  <div className="flex flex-row gap-2 items-start justify-start w-[58%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      20%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      L’art de la création
                    </Text>
                  </div>
                  <div className="flex flex-row gap-3 items-start justify-start w-[79%] md:w-full">
                    <Text
                      className="text-lg text-orange-A200 text-shadow-ts"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      10%
                    </Text>
                    <Text
                      className="mt-[3px] text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Partagez des faits amusant
                    </Text>
                  </div>
                  <div className="flex flex-row gap-[21px] items-start justify-start w-[41%] md:w-full">
                    <Text
                      className="h-[26px] text-lg text-orange-A200 text-shadow-ts w-[26px]"
                      size="txtMontserratRomanSemiBold18OrangeA200"
                    >
                      5%
                    </Text>
                    <Text
                      className="mt-0.5 text-base text-blue_gray-800"
                      size="txtMontserratRomanRegular16Bluegray800"
                    >
                      Avis clients
                    </Text>
                  </div>
                </div>
              </div>
            </div>
          </List>
          <Text
            className="bg-clip-text bg-gradient  mt-[74px] text-transparent text-xl"
            size="txtMontserratRomanSemiBold20Orange200"
          >
            Je vous laisse choisir, peut importe
          </Text>
          <footer className="flex font-leaguespartan items-center justify-center mt-[100px] md:px-5 w-full">
            <div className="bg-blue_gray-800 flex flex-col items-center justify-center pt-[52px] rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full">
              <div className="flex flex-col gap-[47px] items-center justify-center w-full">
                <div className="flex flex-col items-start justify-start ml-20 md:ml-[0] mr-[982px] w-[27%] md:w-full">
                  <Text
                    className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
                    size="txtLeagueSpartanBold32"
                  >
                    Create for me
                  </Text>
                  <div className="flex flex-col font-montserrat gap-[37px] items-start justify-start mt-[45px] w-full">
                    <Text
                      className="text-orange-50 text-sm"
                      size="txtMontserratRomanBold14"
                    >
                      Enterprise
                    </Text>
                    <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Contactor
                      </Text>
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Les avis
                      </Text>
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Programmer d’affiliation
                      </Text>
                    </div>
                  </div>
                  <Img
                    className="h-[22px] md:h-auto mt-[37px] object-cover"
                    src="images/img_group688.png"
                    alt="group688"
                  />
                </div>
                <div className="bg-blue_gray-800 border-blue_gray-100 border-solid border-t flex flex-col items-start justify-start p-[30px] sm:px-5 shadow-bs1 w-full">
                  <div className="flex flex-col items-center justify-start mb-[7px] md:ml-[0] ml-[49px]">
                    <Text
                      className="text-sm text-white-A700"
                      size="txtLeagueSpartanMedium14"
                    >
                      Copyright@2023createforme
                    </Text>
                  </div>
                </div>
              </div>
            </div>
          </footer>
        </div>
      </div>
    </>
  );
};

export default ChoisislastratgiequitecorrespondslaplusPage;
