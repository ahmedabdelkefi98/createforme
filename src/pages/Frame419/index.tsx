import React from "react";

import { Img } from "components";

const Frame419Page: React.FC = () => {
  return (
    <>
      <div className="bg-black-900_01 flex sm:flex-col md:flex-col flex-row sm:gap-10 md:gap-10 gap-[60px] items-center mx-auto md:pr-10 sm:pr-5 pr-[52px] w-full">
        <Img
          className="h-[700px] sm:h-auto object-cover w-[63%] md:w-full"
          src="images/img_desktop62.png"
          alt="desktopSixtyTwo"
        />
        <div className="flex flex-col gap-3.5 items-center justify-start shadow-bs w-[32%] md:w-full">
          <Img
            className="h-[310px] md:h-auto object-cover w-full"
            src="images/img_choisislastratgie.png"
            alt="choisislastratg"
          />
          <Img
            className="h-[321px] md:h-auto object-cover w-full"
            src="images/img_createforme2.png"
            alt="createformeTwo"
          />
        </div>
      </div>
    </>
  );
};

export default Frame419Page;
