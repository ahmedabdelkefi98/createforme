import React from "react";

import { Button, Img, Text } from "components";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const FlicitationPage: React.FC = () => {
  return (
    <>
      <div className="bg-white-A700 flex flex-col items-center justify-start mx-auto w-full">
        <div className="font-leaguespartan md:h-[58px] h-[60px] md:px-5 relative w-full">
          <Text
            className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
            size="txtLeagueSpartanMedium20"
          >
            Découvrez la stratégie infaillible pour vendre vos produits grâce
            aux shortvidéos
          </Text>
          <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
            <Text
              className="mb-[5px] text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
          </div>
        </div>
        <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row font-leaguespartan md:gap-5 items-center justify-start p-3.5 w-full">
          <Img
            className="h-12 w-12"
            src="images/img_iconparkhamburgerbutton.svg"
            alt="iconparkhamburg"
          />
          <Text
            className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
            size="txtLeagueSpartanBold24"
          >
            create for me
          </Text>
          <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
          <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
        </div>
        <div className="font-montserrat md:h-[115px] h-[63px] mt-[61px] md:px-5 relative w-[15%]">
          <Img
            className="absolute h-[54px] left-[0] top-[0] w-[54px]"
            src="images/img_fireworks.svg"
            alt="fireworks"
          />
          <Text
            className="absolute bottom-[0] right-[0] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
            size="txtMontserratRomanSemiBold30"
          >
            Félicitation
          </Text>
        </div>
        <Text
          className="mt-[61px] text-2xl md:text-[22px] text-teal-400 sm:text-xl"
          size="txtMontserratRomanSemiBold24Teal400"
        >
          Voici la stratégie que nous allons utiliser désormais
        </Text>
        <div className="font-montserrat h-[1285px] md:h-[1300px] sm:h-[1451px] mt-[42px] md:px-5 relative w-full">
          <div className="absolute bg-gradient3  flex flex-col h-max inset-[0] items-center justify-center m-auto orange_200_blue_gray_800_border15 outline outline-[1px] p-[42px] md:px-10 sm:px-5 w-full">
            <div className="flex flex-col md:gap-10 gap-[84px] items-center justify-start mb-[405px] w-[67%] md:w-full">
              <div className="flex flex-col gap-[37px] items-center justify-start w-full">
                <Text
                  className="text-2xl md:text-[22px] text-blue_gray-800 sm:text-xl"
                  size="txtMontserratRomanSemiBold24"
                >
                  Stratégie 1 - Le Sushi du Jour{" "}
                </Text>
                <div className="flex flex-col gap-[46px] items-start justify-start w-full">
                  <div className="flex flex-col items-center justify-start md:ml-[0] ml-[5px] w-full">
                    <div className="flex sm:flex-col flex-row gap-4 items-start justify-start w-full">
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanSemiBold18Bluegray800"
                      >
                        1.
                      </Text>
                      <Text
                        className="leading-[142.01%] text-blue_gray-800 text-lg"
                        size="txtMontserratRomanMedium18"
                      >
                        <>
                          Le Sushi du Jour (30%) : Chaque jour, mettez en avant
                          un plat de sushi spécial en le présentant <br />
                          comme le &quot;Sushi du Jour*. Fournissez une
                          description alléchante et des photos
                        </>
                      </Text>
                    </div>
                  </div>
                  <div className="flex md:flex-col flex-row gap-4 items-start justify-start w-[99%] md:w-full">
                    <Text
                      className="text-blue_gray-800 text-lg"
                      size="txtMontserratRomanSemiBold18Bluegray800"
                    >
                      2.
                    </Text>
                    <Text
                      className="leading-[142.01%] text-blue_gray-800 text-lg"
                      size="txtMontserratRomanMedium18"
                    >
                      <>
                        Cuisine en Direct (20%): Organisez des sessions de
                        cuisine en direct où les chefs préparent en <br />
                        direct un plat de sushi spécial du jour, impliquant
                        ainsi le public dans le processus.
                      </>
                    </Text>
                  </div>
                  <div className="flex md:flex-col flex-row gap-4 items-start justify-start ml-0.5 md:ml-[0] w-[97%] md:w-full">
                    <Text
                      className="text-blue_gray-800 text-lg"
                      size="txtMontserratRomanSemiBold18Bluegray800"
                    >
                      3.
                    </Text>
                    <Text
                      className="leading-[142.01%] text-blue_gray-800 text-lg"
                      size="txtMontserratRomanMedium18"
                    >
                      <>
                        Concours de Recettes (15%) : Lancez un concours de
                        recettes où les clients soumettent leurs <br />
                        propres créations de sushis. Partagez les meilleures
                        recettes et impliquez les
                      </>
                    </Text>
                  </div>
                  <div className="flex flex-col items-center justify-start w-[94%] md:w-full">
                    <div className="flex sm:flex-col flex-row gap-4 items-start justify-start w-full">
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanSemiBold18Bluegray800"
                      >
                        4.
                      </Text>
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanMedium18"
                      >
                        <>
                          L&#39;Art des Sushis (20%) : Montrez l&#39;art de la
                          création de sushis en partageant des vidéos de
                        </>
                      </Text>
                    </div>
                  </div>
                  <div className="flex flex-col items-center justify-start ml-0.5 md:ml-[0] w-[85%] md:w-full">
                    <div className="flex sm:flex-col flex-row gap-4 items-start justify-start w-full">
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanSemiBold18Bluegray800"
                      >
                        5.
                      </Text>
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanMedium18"
                      >
                        Le Saviez-Vous ? (10%): Partagez des faits amusants et
                        intéressants sur la cuisine
                      </Text>
                    </div>
                  </div>
                  <div className="flex flex-col items-center justify-start w-[57%] md:w-full">
                    <div className="flex sm:flex-col flex-row gap-3.5 items-start justify-start w-full">
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanSemiBold18Bluegray800"
                      >
                        6.
                      </Text>
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanMedium18"
                      >
                        <>
                          japonaise. les sushis. ou l&#39;histoire de votre
                          restaurant
                        </>
                      </Text>
                    </div>
                  </div>
                  <div className="flex flex-col items-center justify-start ml-0.5 md:ml-[0] w-[89%] md:w-full">
                    <div className="flex sm:flex-col flex-row gap-[15px] items-start justify-start w-full">
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanSemiBold18Bluegray800"
                      >
                        7.
                      </Text>
                      <Text
                        className="text-blue_gray-800 text-lg"
                        size="txtMontserratRomanMedium18"
                      >
                        Avis Clients (5%) : Partagez des avis clients positifs
                        ou des histoires de clients fidèles.
                      </Text>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="bg-cover bg-no-repeat flex flex-col font-inter h-[55px] items-center justify-end p-3.5"
                style={{ backgroundImage: "url('images/img_group622.svg')" }}
              >
                <Text
                  className="text-black-900 text-xl"
                  size="txtInterRegular20"
                >
                  Je suis prêt(e) à voir mon planning
                </Text>
              </div>
            </div>
          </div>
          <footer className="absolute bottom-[0] flex font-leaguespartan inset-x-[0] items-center justify-center mx-auto w-full">
            <div className="md:h-[387px] h-[392px] relative w-full">
              <div className="bg-blue_gray-800 flex flex-col h-full items-start justify-start m-auto p-[52px] md:px-10 sm:px-5 rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full">
                <div className="flex flex-col items-start justify-start mb-[75px] md:ml-[0] ml-[33px] w-[29%] md:w-full">
                  <Text
                    className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
                    size="txtLeagueSpartanBold32"
                  >
                    Create for me
                  </Text>
                  <div className="flex flex-col font-montserrat gap-[37px] items-start justify-start mt-[45px] w-full">
                    <Text
                      className="text-orange-50 text-sm"
                      size="txtMontserratRomanBold14"
                    >
                      Enterprise
                    </Text>
                    <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Contactor
                      </Text>
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Les avis
                      </Text>
                      <Text
                        className="text-orange-50 text-sm"
                        size="txtMontserratRomanMedium14"
                      >
                        Programmer d’affiliation
                      </Text>
                    </div>
                  </div>
                  <Img
                    className="h-[22px] md:h-auto mt-[37px] object-cover"
                    src="images/img_group688.png"
                    alt="group688"
                  />
                </div>
              </div>
              <Text
                className="absolute bg-blue_gray-800 border-blue_gray-100 border-solid border-t bottom-[0] h-[81px] inset-x-[0] justify-center max-w-[1439px] md:max-w-full mx-auto pb-[35px] pt-[30px] sm:px-5 px-[35px] text-shadow-ts1 text-sm text-white-A700 w-max"
                size="txtLeagueSpartanMedium14"
              >
                Copyright@2023createforme
              </Text>
            </div>
          </footer>
        </div>
      </div>
    </>
  );
};

export default FlicitationPage;
