import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "pages/Home";
import NotFound from "pages/NotFound";
const Frame419 = React.lazy(() => import("pages/Frame419"));
const Flicitation = React.lazy(() => import("pages/Flicitation"));
const Choisislastratgiequitecorrespondslaplus = React.lazy(
  () => import("pages/Choisislastratgiequitecorrespondslaplus"),
);
const Etape4Five = React.lazy(() => import("pages/Etape4Five"));
const Etape1Five = React.lazy(() => import("pages/Etape1Five"));
const Jecrermastratgie = React.lazy(() => import("pages/Jecrermastratgie"));
const Connexion = React.lazy(() => import("pages/Connexion"));
const CreateformeTwo = React.lazy(() => import("pages/CreateformeTwo"));
const HomepageOne = React.lazy(() => import("pages/HomepageOne"));
const Homepage = React.lazy(() => import("pages/Homepage"));
const ProjectRoutes = () => {
  return (
    <React.Suspense fallback={<>Loading...</>}>
      <Router>
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/homepageone" element={<HomepageOne />} />
          <Route path="/createformetwo" element={<CreateformeTwo />} />
          <Route path="/connexion" element={<Connexion />} />
          <Route path="/jecrermastratgie" element={<Jecrermastratgie />} />
          <Route path="/etape1five" element={<Etape1Five />} />
          <Route path="/etape4five" element={<Etape4Five />} />
          <Route
            path="/choisislastratgiequitecorrespondslaplus"
            element={<Choisislastratgiequitecorrespondslaplus />}
          />
          <Route path="/flicitation" element={<Flicitation />} />
          <Route path="/frame419" element={<Frame419 />} />
          <Route path="/dhiwise-dashboard" element={<Home />} />
        </Routes>
      </Router>
    </React.Suspense>
  );
};
export default ProjectRoutes;
