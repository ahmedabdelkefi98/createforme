import React from "react";

import { useNavigate } from "react-router-dom";

import { Button, Img, Text } from "components";
import HomepageColumn from "components/HomepageColumn";
import HomepageColumnlogin from "components/HomepageColumnlogin";

const HomepagePage: React.FC = () => {
  const navigate = useNavigate();

  return (
    <>
      <div className="bg-white-A700 flex flex-col font-leaguespartan items-center justify-end mx-auto w-full">
        <div className="flex flex-col items-center justify-end w-full">
          <div className="md:h-[58px] h-[60px] md:px-5 relative w-full">
            <Text
              className="mt-[17px] mx-auto text-center text-white-A700 text-xl"
              size="txtLeagueSpartanMedium20"
            >
              Découvrez la stratégie infaillible pour vendre vos produits grâce
              aux shortvidéos
            </Text>
            <div className="absolute bg-gradient  flex flex-col h-full inset-[0] items-center justify-center m-auto p-[17px] w-full">
              <Text
                className="mb-[5px] text-center text-white-A700 text-xl"
                size="txtLeagueSpartanMedium20"
              >
                Découvrez la stratégie infaillible pour vendre vos produits
                grâce aux shortvidéos
              </Text>
            </div>
          </div>
          <div className="border-b border-blue_gray-100 border-solid flex md:flex-col flex-row md:gap-5 items-center justify-start p-3.5 w-full">
            <Img
              className="common-pointer h-12 w-12"
              src="images/img_iconparkhamburgerbutton.svg"
              alt="iconparkhamburg"
              onClick={() => navigate("/homepageone")}
            />
            <Text
              className="md:ml-[0] ml-[17px] text-2xl md:text-[22px] text-blue_gray-800 text-center sm:text-xl"
              size="txtLeagueSpartanBold24"
            >
              create for me
            </Text>
            <HomepageColumn className="flex md:flex-1 flex-col font-montserrat items-center justify-start md:ml-[0] ml-[807px] md:px-5 w-[4%] md:w-full" />
            <HomepageColumnlogin className="flex md:flex-1 flex-col font-montserrat items-center justify-start ml-5 md:ml-[0] md:mt-0 mt-[3px] md:px-5 w-[19%] md:w-full" />
          </div>
          <div className="flex md:flex-col flex-row md:gap-10 items-start justify-between max-w-7xl mt-[52px] mx-auto md:px-5 w-full">
            <div className="flex md:flex-1 flex-col items-start justify-start md:mt-0 mt-16 w-[48%] md:w-full">
              <div className="flex flex-col font-leaguespartan gap-7 items-center justify-start w-full">
                <Text
                  className="sm:text-[41px] md:text-[47px] text-[55px] text-blue_gray-800"
                  size="txtLeagueSpartanBold55"
                >
                  <>
                    Votre stratégie détaillée <br />à porter de main
                  </>
                </Text>
                <Text
                  className="text-gray-800 text-xl"
                  size="txtLeagueSpartanRegular20"
                >
                  <>
                    C’est le moment de revenir au cœur de votre métier en vous
                    déchargeant <br />
                    de la partie communication pour enfin vivre de votre
                    activité.
                  </>
                </Text>
              </div>
              <Button
                className="cursor-pointer font-medium font-montserrat leading-[normal] min-w-[326px] mt-8 text-center text-xl"
                shape="round"
                color="orange_A200"
                size="sm"
                variant="fill"
              >
                Créez votre compte gratuit
              </Button>
              <div className="flex flex-col font-montserrat items-center justify-start mt-[38px] w-[44%] md:w-full">
                <div className="flex flex-col gap-2 items-start justify-start w-full">
                  <Text
                    className="text-2xl md:text-[22px] text-center text-teal-400 sm:text-xl"
                    size="txtMontserratRomanMedium24"
                  >
                    Il nous font confiance
                  </Text>
                  <div className="flex flex-row items-center justify-start w-[58%] md:w-full">
                    <Img
                      className="h-[39px] w-[39px]"
                      src="images/img_settings.svg"
                      alt="settings"
                    />
                    <Button
                      className="flex h-[26px] items-center justify-center ml-2.5 my-1.5 w-[26px]"
                      shape="circle"
                      color="teal_200"
                      size="xs"
                      variant="fill"
                    >
                      <Img src="images/img_vector.svg" alt="vector" />
                    </Button>
                    <Img
                      className="h-[30px] md:h-auto ml-2.5 object-cover w-[30px]"
                      src="images/img_chatgpt.png"
                      alt="chatgpt"
                    />
                    <Img
                      className="h-[26px] ml-2.5 w-[25px]"
                      src="images/img_google.svg"
                      alt="google"
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="md:h-[472px] h-[496px] relative w-[41%] md:w-full">
              <div className="absolute border-[7px] border-black-900 border-solid h-[456px] inset-x-[0] mx-auto top-[0] w-full"></div>
              <Img
                className="absolute bottom-[0] flex h-[472px] inset-x-[0] mx-auto object-cover w-[472px]"
                src="images/img_bloggerusingt.png"
                alt="bloggerusingt"
              />
            </div>
          </div>
          <div className="h-[646px] md:h-[668px] mt-[22px] md:px-5 relative w-[95%] md:w-full">
            <div className="absolute flex flex-col font-montserrat gap-[26px] justify-start right-[0] top-[18%] w-[64%]">
              <Text
                className="leading-[142.01%] mr-[7px] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800"
                size="txtMontserratRomanSemiBold30"
              >
                <>
                  Tu es dépasser par tes multiples tâche d’entrepreneur ?<br />
                  Et tu as besoin de déléguer de manière optimal ?
                </>
              </Text>
              <div className="flex flex-col gap-1.5 items-start justify-start md:ml-[0] ml-[242px] w-[73%] md:w-full">
                <Text
                  className="text-teal-400 text-xl"
                  size="txtMontserratRomanMedium20"
                >
                  <>
                    Voici les solutions que nous vous apportons pouréconomiser
                    <br />
                    du temps, de l’énergie et des ressources :
                  </>
                </Text>
                <div className="flex flex-col items-center justify-start md:ml-[0] ml-[3px] rotate-[180deg] w-4/5 md:w-full">
                  <div className="flex flex-col items-center justify-start w-full">
                    <div className="flex flex-col items-center justify-start w-full">
                      <div className="flex flex-col items-center justify-start w-full">
                        <div className="flex flex-col items-center justify-start w-full">
                          <div className="flex flex-col items-start justify-start w-full">
                            <div className="flex sm:flex-col flex-row gap-[9px] items-start justify-between w-full">
                              <Img
                                className="h-[17px] md:h-auto object-cover"
                                src="images/img_vector_17x20.png"
                                alt="vector_One"
                              />
                              <div className="flex flex-col items-center justify-start rotate-[180deg]">
                                <Text
                                  className="text-base text-gray-800 text-right"
                                  size="txtMontserratRomanMedium16"
                                >
                                  Stratégie personnalisée avec planning de post
                                  sur mesure
                                </Text>
                              </div>
                            </div>
                            <div className="flex flex-row gap-[9px] items-start justify-start mt-[22px] rotate-[180deg] w-[66%] md:w-full">
                              <Img
                                className="h-[17px] md:h-auto object-cover"
                                src="images/img_vector_1.png"
                                alt="vector_Two"
                              />
                              <Text
                                className="text-base text-gray-800 text-right"
                                size="txtMontserratRomanMedium16"
                              >
                                Choix de tes sous-titre personnalisés
                              </Text>
                            </div>
                            <div className="flex flex-col items-center justify-start mt-[19px] rotate-[180deg] w-[53%] md:w-full">
                              <div className="flex flex-row gap-[9px] items-start justify-start w-full">
                                <Img
                                  className="h-[17px] md:h-auto object-cover"
                                  src="images/img_vector_2.png"
                                  alt="vector_Three"
                                />
                                <Text
                                  className="mt-0.5 text-base text-gray-800 text-right"
                                  size="txtMontserratRomanMedium16"
                                >
                                  Montage vidéo fait à ta place
                                </Text>
                              </div>
                            </div>
                            <div className="flex sm:flex-col flex-row gap-[9px] items-start justify-start mt-[21px] rotate-[180deg] w-[82%] md:w-full">
                              <Img
                                className="h-[17px] md:h-auto object-cover"
                                src="images/img_vector_3.png"
                                alt="vector_Four"
                              />
                              <Text
                                className="text-base text-gray-800 text-right"
                                size="txtMontserratRomanMedium16"
                              >
                                Caption prêt pour chacune de tes publications
                              </Text>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Text
              className="absolute bottom-[0] right-[30%] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800 text-right"
              size="txtLeagueSpartanSemiBold30"
            >
              Comment ça fonctionne?{" "}
            </Text>
            <Img
              className="absolute h-[646px] inset-y-[0] left-[0] my-auto object-cover rounded-[323px] w-[45%]"
              src="images/img_82635994clock12.png"
              alt="82635994clockTwelve"
            />
          </div>
          <Text
            className="mt-[7px] text-right text-teal-400 text-xl"
            size="txtLeagueSpartanMedium20Teal400"
          >
            Ta stratégie personnalisée est élaborée en fonction de plusieurs
            éléments clés :
          </Text>
          <div className="flex md:flex-col flex-row font-montserrat gap-[25px] items-center justify-start max-w-[1281px] mt-[54px] mx-auto md:px-5 w-full">
            <div className="h-[270px] md:h-[886px] relative w-3/4 md:w-full">
              <div className="flex flex-col h-full items-center justify-start m-auto w-full">
                <div className="flex md:flex-col flex-row md:gap-10 items-center justify-between w-full">
                  <div className="bg-white-A700 border border-orange-A200 border-solid flex md:flex-1 flex-col gap-[17px] items-center justify-center p-4 rounded-bl-[20px] rounded-tr-[20px] shadow-bs w-[32%] md:w-full">
                    <Img
                      className="h-[58px] md:h-auto mt-3 object-cover w-[58px]"
                      src="images/img_ictwotonepubl.png"
                      alt="ictwotonepubl"
                    />
                    <div className="flex flex-col gap-[33px] items-center justify-start mb-[27px]">
                      <Text
                        className="text-black-900 text-center text-lg"
                        size="txtMontserratRomanSemiBold18"
                      >
                        Vos résultats récents de publication
                      </Text>
                      <Text
                        className="leading-[142.01%] text-base text-center text-gray-800"
                        size="txtMontserratRomanRegular16"
                      >
                        <>
                          nous permettant de comprendre <br />
                          ce qui fonctionne le mieux <br />
                          pour votre audience
                        </>
                      </Text>
                    </div>
                  </div>
                  <div className="bg-white-A700 border border-orange-A200 border-solid flex md:flex-1 flex-col items-center justify-start p-[33px] sm:px-5 rounded-bl-[20px] rounded-tr-[20px] shadow-bs w-[32%] md:w-full">
                    <Img
                      className="h-12 md:h-auto mb-[155px] object-cover w-12"
                      src="images/img_octicongoal16.png"
                      alt="octicongoalSixteen"
                    />
                  </div>
                  <div
                    className="bg-cover bg-no-repeat flex md:flex-1 flex-col h-[270px] items-center justify-start p-[7px] w-[32%] md:w-full"
                    style={{ backgroundImage: "url('images/img_group40.svg')" }}
                  >
                    <div className="flex flex-col gap-[47px] items-center justify-start mb-[37px] w-[90%] md:w-full">
                      <Img
                        className="h-[47px] md:h-auto object-cover w-12 sm:w-full"
                        src="images/img_group.png"
                        alt="group"
                      />
                      <div className="flex flex-col gap-[31px] items-center justify-start w-full">
                        <Text
                          className="text-black-900 text-center text-lg"
                          size="txtMontserratRomanSemiBold18"
                        >
                          Analyse de vos concurrents
                        </Text>
                        <Text
                          className="leading-[142.01%] text-base text-center text-gray-800"
                          size="txtMontserratRomanRegular16"
                        >
                          <>
                            afin de saisir les opportunités et <br />
                            de se démarquer dans votre <br />
                            secteur
                          </>
                        </Text>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="absolute bottom-[17%] flex flex-col gap-8 inset-x-[0] items-center justify-start mx-auto">
                <Text
                  className="text-black-900 text-center text-lg"
                  size="txtMontserratRomanSemiBold18"
                >
                  La clarté de vos objectifs
                </Text>
                <Text
                  className="leading-[142.01%] text-base text-center text-gray-800"
                  size="txtMontserratRomanRegular16"
                >
                  <>
                    après vos réponses nous seront <br />
                    alignée sur vos aspirations et <br />
                    vos ambitions
                  </>
                </Text>
              </div>
            </div>
            <div className="md:h-[269px] h-[270px] relative w-[24%] md:w-full">
              <div className="bg-white-A700 border border-orange-A200 border-solid flex flex-col h-full items-center justify-start m-auto p-7 sm:px-5 rounded-bl-[20px] rounded-tr-[20px] shadow-bs w-full">
                <Img
                  className="h-12 md:h-auto mb-[165px] object-cover w-12"
                  src="images/img_vector_48x48.png"
                  alt="vector_Five"
                />
              </div>
              <div className="absolute bottom-[8%] flex flex-col gap-8 inset-x-[0] items-center justify-start mx-auto">
                <Text
                  className="text-black-900 text-center text-lg"
                  size="txtMontserratRomanSemiBold18"
                >
                  Notre expertise
                </Text>
                <Text
                  className="leading-[142.01%] text-base text-center text-gray-800"
                  size="txtMontserratRomanRegular16"
                >
                  <>
                    utilisation de nos stratégie, <br />
                    garantissant une approche sur mesure pour maximiser votre{" "}
                    <br />
                    succès
                  </>
                </Text>
              </div>
            </div>
          </div>
          <Text
            className="leading-[142.01%] mt-[139px] md:text-3xl sm:text-[28px] text-[32px] text-blue_gray-800 text-center w-[84%] sm:w-full"
            size="txtLeagueSpartanSemiBold32"
          >
            Choisissez vos sous-titres parmi nos options existantes ou laissez
            nous vous recommander les meilleurs. Personnalisez votre expérience
            en toute simplicité
          </Text>
          <div className="flex md:flex-col flex-row font-leaguespartan md:gap-5 items-start justify-start max-w-[1270px] mt-[135px] mx-auto md:px-5 w-full">
            <div className="md:h-[325px] h-[345px] relative w-[24%] md:w-full">
              <Img
                className="absolute h-[325px] inset-x-[0] mx-auto object-cover rounded-[20px] top-[0] w-full"
                src="images/img_rectangle25.png"
                alt="rectangleTwentyFive"
              />
              <Text
                className="absolute bottom-[0] inset-x-[0] mx-auto text-4xl sm:text-[32px] md:text-[34px] text-center text-white-A700"
                size="txtLeagueSpartanSemiBold36"
              >
                <>
                  bienvenue dans <br />
                  notre monde
                </>
              </Text>
            </div>
            <div className="flex ml-2 md:ml-[0] relative w-[51%] md:w-full">
              <div className="h-[325px] my-auto w-[56%]">
                <Img
                  className="absolute h-[325px] inset-[0] justify-center m-auto object-cover rounded-[20px] w-[84%]"
                  src="images/img_rectangle25_325x295.png"
                  alt="rectangleTwentyFive_One"
                />
                <Text
                  className="absolute bottom-[5%] inset-x-[0] mx-auto text-4xl sm:text-[32px] md:text-[34px] text-center text-white-A700"
                  size="txtLeagueSpartanSemiBold36"
                >
                  <>
                    Voyagez à <br />
                    travers le temps
                  </>
                </Text>
              </div>
              <div className="flex flex-col items-center justify-start ml-[-6px] my-auto w-[46%] z-[1]">
                <div className="h-[325px] relative w-full">
                  <Img
                    className="h-[325px] m-auto object-cover rounded-[20px] w-full"
                    src="images/img_rectangle25_1.png"
                    alt="rectangleTwentyFive_Two"
                  />
                  <Text
                    className="absolute bottom-[6%] inset-x-[0] mx-auto text-4xl sm:text-[32px] md:text-[34px] text-center text-white-A700 w-[84%] sm:w-full"
                    size="txtLeagueSpartanSemiBold36"
                  >
                    Paris romantique
                  </Text>
                </div>
              </div>
            </div>
            <div className="flex md:flex-1 flex-col items-center justify-start ml-7 md:ml-[0] w-[24%] md:w-full">
              <div className="h-[325px] relative w-full">
                <Img
                  className="h-[325px] m-auto object-cover rounded-[20px] w-full"
                  src="images/img_rectangle25_2.png"
                  alt="rectangleTwentyFive_Three"
                />
                <Text
                  className="absolute bottom-[8%] right-[6%] text-4xl sm:text-[32px] md:text-[34px] text-center text-white-A700"
                  size="txtLeagueSpartanSemiBold36"
                >
                  <>
                    La passion de <br />
                    l&#39;art à l&#39;état pur
                  </>
                </Text>
              </div>
            </div>
          </div>
          <Text
            className="capitalize mt-[134px] text-3xl sm:text-[26px] md:text-[28px] text-blue_gray-800 text-center"
            size="txtLeagueSpartanBold30"
          >
            Le Montage Vide’o
          </Text>
          <div className="font-leaguespartan md:h-[104px] h-[74px] max-w-[1144px] mt-[30px] mx-auto md:px-5 relative w-full">
            <Text
              className="m-auto text-center text-gray-800 text-xl"
              size="txtLeagueSpartanMedium20Gray800"
            >
              <>
                Libérez-vous de la créativité stressante. Il vous suffit
                d&#39;envoyer les vidéos que nous vous demandons, et nous nous
                chargeons du reste. <br />
                Nos montages vidéo automatiques, intégrés à notre stratégie avec
                CapCut, préservent votre authenticité sans que vous <br />
                ayez à vous soucier du processus créatif
              </>
            </Text>
            <Img
              className="absolute bottom-[11%] h-[27px] left-[21%] object-cover"
              src="images/img_vector_27x29.png"
              alt="vector_Six"
            />
          </div>
          <Text
            className="mt-[29px] text-[22px] text-center text-gray-800 sm:text-lg md:text-xl"
            size="txtLeagueSpartanMedium22"
          >
            Imagine pouvoir gagner du temps et de l’argent sur ton marketing{" "}
          </Text>
          <div className="flex flex-col font-leaguespartan items-center justify-start mt-[53px] md:px-5 w-[18%] md:w-full">
            <Button
              className="!text-blue_gray-800 cursor-pointer font-medium leading-[normal] min-w-[256px] text-center text-xl"
              shape="round"
              color="orange_A200"
              size="md"
              variant="fill"
            >
              commencer à éditer
            </Button>
          </div>
          <div
            className="bg-cover bg-no-repeat flex flex-col font-leaguespartan h-[782px] items-center justify-start mt-[127px] p-[89px] md:px-5 w-[53%] md:w-full"
            style={{ backgroundImage: "url('images/img_group38.png')" }}
          >
            <div className="flex flex-col md:gap-10 gap-[168px] items-center justify-start mb-[178px]">
              <Text
                className="text-4xl sm:text-[32px] md:text-[34px] text-white-A700"
                size="txtLeagueSpartanBold36"
              >
                Caption
              </Text>
              <Text
                className="capitalize text-center text-white-A700 text-xl"
                size="txtLeagueSpartanMedium20"
              >
                <>
                  Optimisez votre visibilité avec notre service de <br />
                  création de captions sur mesure. Nous vous <br />
                  proposons non pas 1, mais 3 textes de légende <br />
                  uniques, accompagnés des hashtags pertinents <br />
                  et des emojis adéquats. Libérez-vous du stress <br />
                  de la rédaction, nous nous occupons de tout <br />
                  pour maximiser l&#39;impact de votre contenu.
                </>
              </Text>
            </div>
          </div>
          <div className="flex font-leaguespartan h-[451px] md:h-[577px] justify-end mt-[126px] md:px-5 relative w-full">
            <Text
              className="absolute bottom-[0] left-[6%] text-center text-sm text-white-A700"
              size="txtLeagueSpartanMedium14"
            >
              Copyright@2023createforme
            </Text>
            <Text
              className="mb-2 ml-[72px] mt-auto text-sm text-white-A700"
              size="txtLeagueSpartanMedium14"
            >
              Copyright@2023createforme
            </Text>
            <footer className="absolute flex inset-[0] items-center justify-center m-auto w-full">
              <div className="flex flex-col gap-[46px] items-center justify-center w-full">
                <div className="flex md:h-[387px] h-[392px] justify-end relative w-full">
                  <div className="flex flex-row gap-[26px] h-full items-center justify-center mb-3.5 ml-[83px] mt-auto w-[6%]">
                    <Img
                      className="h-[30px]"
                      src="images/img_logostiktokicon.svg"
                      alt="logostiktokicon"
                    />
                    <Img
                      className="h-[30px] md:h-auto object-cover w-[30px]"
                      src="images/img_group_white_a700.png"
                      alt="group_One"
                    />
                  </div>
                  <div className="absolute bg-blue_gray-800 flex flex-col h-full inset-[0] items-center justify-center m-auto pt-[52px] rounded-tl-[53px] rounded-tr-[53px] shadow-bs1 w-full">
                    <div className="flex flex-col gap-[47px] justify-start w-full">
                      <div className="flex flex-col items-start justify-start md:ml-[0] ml-[78px] w-[27%] md:w-full">
                        <Text
                          className="md:text-3xl sm:text-[28px] text-[32px] text-orange-50"
                          size="txtLeagueSpartanBold32"
                        >
                          Create for me
                        </Text>
                        <div className="flex flex-col font-montserrat gap-[37px] items-start justify-start mt-[45px] w-full">
                          <Text
                            className="text-orange-50 text-sm"
                            size="txtMontserratRomanBold14"
                          >
                            Enterprise
                          </Text>
                          <div className="flex flex-row gap-[39px] items-start justify-between w-full">
                            <Text
                              className="text-orange-50 text-sm"
                              size="txtMontserratRomanMedium14"
                            >
                              Contactor
                            </Text>
                            <Text
                              className="text-orange-50 text-sm"
                              size="txtMontserratRomanMedium14"
                            >
                              Les avis
                            </Text>
                            <Text
                              className="text-orange-50 text-sm"
                              size="txtMontserratRomanMedium14"
                            >
                              Programmer d’affiliation
                            </Text>
                          </div>
                        </div>
                        <Img
                          className="h-[22px] md:h-auto mt-[37px] object-cover"
                          src="images/img_group688.png"
                          alt="group688"
                        />
                      </div>
                      <div className="bg-blue_gray-800 border-blue_gray-100 border-solid border-t flex flex-col items-start justify-start p-[30px] sm:px-5 shadow-bs1 w-full">
                        <div className="flex flex-col items-center justify-start mb-[7px] md:ml-[0] ml-[47px]">
                          <Text
                            className="text-sm text-white-A700"
                            size="txtLeagueSpartanMedium14"
                          >
                            Copyright@2023createforme
                          </Text>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <Text
                  className="md:ml-[0] ml-[94px] mr-[1172px] text-center text-sm text-white-A700"
                  size="txtLeagueSpartanMedium14"
                >
                  Copyright@2023createforme
                </Text>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </>
  );
};

export default HomepagePage;
