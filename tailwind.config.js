module.exports = {
  mode: "jit",
  content: [
    "./src/**/**/*.{js,ts,jsx,tsx,html,mdx}",
    "./src/**/*.{js,ts,jsx,tsx,html,mdx}",
  ],
  darkMode: "class",
  theme: {
    screens: { md: { max: "1050px" }, sm: { max: "550px" } },
    extend: {
      colors: {
        cyan: { A400: "#00f2ea" },
        blue_gray: { 100: "#d9d9d9", 800: "#2f4858", "100_00": "#d9d9d900" },
        red: {
          300: "#f37168",
          500: "#eb4335",
          800: "#c5221f",
          A700: "#ff0000",
          "500_01": "#ea4335",
        },
        amber: { 500: "#fbbc05", "500_01": "#fbbc04", A400: "#fac600" },
        blue: {
          50: "#e7f3fe",
          A200: "#5e94ff",
          "200_00": "#9bd6e800",
          A200_02: "#4285f4",
          A200_01: "#5485e5",
          "200_6b": "#9bd6e86b",
        },
        light_blue: { 400: "#26a6fe" },
        black: { 900: "#000000", "900_3f": "#0000003f", "900_01": "#040a1d" },
        green: { 600: "#34a853" },
        teal: { 200: "#67c4ce", 300: "#46cc8d", 400: "#2d8890" },
        pink: { A400: "#ff004f" },
        orange: {
          50: "#fef2de",
          200: "#f4b96d",
          A200: "#eda64c",
          A200_2b: "#eda64c2b",
        },
        gray: { 300: "#e5dada", 800: "#4f4f4f" },
        white: { A700: "#ffffff" },
        indigo: { 600: "#3d5a98" },
      },
      boxShadow: {
        bs: "0px 4px  4px 0px #0000003f",
        bs2: "4px 4px  4px 0px #0000003f",
        bs1: "8px -1px  4px 0px #0000003f",
      },
      fontFamily: {
        leaguespartan: "League Spartan",
        inter: "Inter",
        montserrat: "Montserrat",
      },
      backgroundImage: {
        gradient: "linear-gradient(270deg ,#f4b96d,#2d8890,#2f4858)",
        gradient1: "linear-gradient(356deg ,#f4b96d,#2d8890,#2f4858)",
        gradient2: "linear-gradient(90deg ,#9bd6e800,#9bd6e86b)",
        gradient3: "linear-gradient(180deg ,#eda64c2b,#d9d9d900)",
      },
      textShadow: {
        ts: "0px 4px  4px #0000003f",
        ts1: "8px -1px  4px #0000003f",
      },
    },
  },
  plugins: [require("@tailwindcss/forms"), require("tailwindcss-textshadow")],
};
